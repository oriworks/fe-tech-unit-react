const url = "http://localhost:3001";
let size = 10;
let currentBoard = [];

function drawBoard(board) {
    currentBoard = board;
    const uiBoard = document.createElement('div');

    board.map((row, x) => {
        const uiRow = document.createElement('div')
        row.map((col, y) => {
            const cell = document.createElement('div');
            cell.setAttribute('data-x', x);
            cell.setAttribute('data-y', y);
            cell.onclick = manuallyUpdateBoard;
            cell.innerHTML = col ? '😎' : ''
            uiRow.appendChild(cell);
        })
        uiBoard.appendChild(uiRow);
    });
    document.getElementById('board').innerHTML = '';
    document.getElementById('board').appendChild(uiBoard);
}

function manuallyUpdateBoard(e) {
    const x = e.target.getAttribute('data-x');
    const y = e.target.getAttribute('data-y');
    const isAlive = e.target.innerHTML !== '';
    currentBoard[x][y] = !isAlive;

    importBoard()
    
}

function importBoard() {
    fetch(`${url}/board/import`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ board: currentBoard })
    }).then(res =>  res.json()).then(drawBoard)
}

function createBoardBtn() {
    size = document.getElementById('boardSize').value;
    type = document.getElementById('board-list').value;
    
    fetch(`${url}/board`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ size, type })
    }).then(res => res.json()).then(drawBoard)
    .catch(error => {
        document.getElementById('board').innerHTML = drawBoard([[0, 1, 0], [0, 1, 0], [0, 1, 0]]);
        document.getElementById('errorMsg').innerHTML = `Could not get board.<br>Error: ${error}`;
    });
}

function next() {
    if (iterations > 0) {
        if (iterations === 1) {
            playStop();
        }
        iterations--;
        document.getElementById('iterations').value = iterations;    
    }
    
    fetch(`${url}/board/next`, {method: 'PUT'}).then(res => res.json()).then(drawBoard);
}

function previous() {
    fetch(`${url}/board/prev`, {method: 'PUT'}).then(res => res.json()).then(drawBBoard);
}


function listBoards() {
    fetch(`${url}/boards`)
        .then(res => res.json())
        .then(res => {
            const select = document.getElementById('board-list');

            select.innerHTML = '';

            const defaultOption = document.createElement('option')
            defaultOption.innerHTML = "Select a board";
            defaultOption.value = '';
            select.appendChild(defaultOption);

            res.map((opt, index) => {
                let newOption = document.createElement('option');
                newOption.innerHTML = opt;
                newOption.value = opt.toLowerCase();
                select.appendChild(newOption)

            })

        });
}

function enableButton(e) {
    size = document.getElementById('boardSize').value;
    type = document.getElementById('board-list').value;
     if (type.length === 0) {
       document.getElementById('create-game').setAttribute("disabled","disabled");
    } else {
       document.getElementById('create-game').removeAttribute("disabled");
    }
}

function playStop() {
    // * Switch button text between Run and Stop
    // * Use the iterations value to limit the number of iterations performed
}

listBoards();