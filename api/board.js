let pervBoard = [];
let board = [];

export const BoardType = {
    EMPTY: 'empty',
    RANDOM: 'random',
    FILL: 'fill',
    CUSTOM: 'custom'
}

const BoardTypeFunctions = {
    [BoardType.EMPTY]: () => false,
    [BoardType.RANDOM]: () => Math.random() < 0.5,
    [BoardType.FILL]: () => true,
    [BoardType.CUSTOM]: (x,y) => (x+y) % 2 === 0
}

export const listBoards = () => {
    return Object.values(BoardType);
}

export const resetBoard = (type = BoardType.EMPTY) => {
    prevStep = []
    board = []
    return board;
}

export const createBoard = (size = 10, type = BoardType.EMPTY) => {
    board = Array.from(
        { length: size },
        (row, i) => Array.from(
            { length: size },
            (cell, j) =>  BoardTypeFunctions[type](i, j)
            
        )
    )
    return board
} 

export const importBoard = (inputBoard) => {
    const newBoard = []
    for (let i = 0; i < inputBoard.length; i++) {
        newBoard.push([])
        for (let j = 0; j < inputBoard[i].length; j++) {
            newBoard[i].push([])
            if (typeof inputBoard[i][j] !== "boolean") {
                return 1; // Error
            }
            newBoard[i][j] = inputBoard[i][j]
        }
    }
    board = newBoard;
    return board; // Success
}

// Suggestion
const countNeighbors = (board, x, y) => {
    const rows = board.length
    const cols = board[rows-1].length
    let sum = board[x][y] ? -1 : 0;

    for (let i = -1; i < 2; i++) {
        const row = x + i
        if(row >= 0 && row < rows) {
            for (let j = -1; j < 2; j++) {
                const col = y + j
                if (col >= 0 && col < cols) {
                    sum += board[row][col] ? 1 : 0
                }
            }
        }
    }
    return sum;
}

// Suggestion
const nextGeneration = (board = [[]]) => {
    const next = [];

    for (let i = 0; i < board.length; i++) {
        next[i] = []
        for (let j = 0; j < board[i].length; j++) {
            const neighbors = countNeighbors(board, i, j);
            if (board[i][j] && (neighbors < 2 || neighbors > 3)) {
                next[i][j] = false
            } else if (!board[i][j] && neighbors === 3) {
                next[i][j] = true
            } else {
                next[i][j] = board[i][j]
            }
        }
    }
    return next;
}

export const actual = () => {
    return board
}
export const nextStep = () => {
    pervBoard.push(board)
    board = nextGeneration(board)
    return board;
}

export const prevStep = () => {
    board = pervBoard.pop()
    return board
}