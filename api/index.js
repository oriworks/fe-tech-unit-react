import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors';
import { createBoard, importBoard, listBoards, nextStep, prevStep, resetBoard, actual } from './board'

var app = express()

app.use(cors());
app.use(bodyParser.json())
const port = 3001

app.get('/boards', function (req, res) {
    res.send(listBoards())
})
app.get('/board', function (req, res) {
    res.send(actual()) 
})
app.post('/board', function (req, res) {
    res.send(createBoard(req.body.size, req.body.type)) 
})
app.get('/board/reset/:type?', function (req, res) {
    res.send(resetBoard(req.params.type))
})
app.post('/board/import', function (req, res) {
    res.send(importBoard(req.body.board)) 
})
app.put('/board/:type', function (req, res) {
    if (req.params.type === 'next') {
        res.send(nextStep())
    } else if (req.params.type === 'prev') {
        res.send(prevStep())
    } else {
        res.status(404).send({ message: 'Method not found' })
    }
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})